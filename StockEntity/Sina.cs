﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace GuPiao
{
    public  class Sina:IDataService
    {
        private const string dataurl = "http://hq.sinajs.cn/list={0}";
        #region IStockInfo Members
        WebClient client;
        private StockInfo PrevInfo;
        private List<StockInfo> ListInfo;

        public StockInfo GetCurrentStockInfo(string stockCode)
        {
            try
            {
                if (client == null)
                {
                    client = new WebClient();
                }
                if (stockCode.Substring(0, 2) == "60")//上海是600打头
                {
                    stockCode = "sh" + stockCode;
                }
                else if (stockCode.Substring(0, 2) == "00")//深圳
                {
                    stockCode = "sz" + stockCode;
                }
                else if (stockCode.Substring(0, 2) == "51")//上海基金
                {
                    stockCode = "sh" + stockCode;
                }
                string url = string.Format(dataurl, stockCode);
                string data = client.DownloadString(string.Format(url, stockCode));
                PrevInfo = Parse(data);
                return PrevInfo;
            }
            catch
            {
                return PrevInfo;
            }
        }

        public List<StockInfo> GetCurrentStockInfo(List<string> stockCode)
        {
            try
            {
                string strcode = "";
                if (client == null)
                {
                    client = new WebClient();
                }
                foreach (string s in stockCode)  //strcode=60011454,45464122,464564564
                {
                    if (!string.IsNullOrEmpty(strcode))
                    {
                        strcode += ",";
                    }

                    if (s.Substring(0, 2) == "60")//上海是600打头
                    {
                        strcode += "sh" + s;
                    }
                    else if (s.Substring(0, 2) == "00")//深圳
                    {
                        strcode += "sz" + s;
                    }
                    else if (s.Substring(0, 2) == "51")//上海基金
                    {
                        strcode += "sh" + s;
                    }
                }
                string url = string.Format(dataurl, stockCode);
                string data = client.DownloadString(string.Format(url, stockCode));

                string[] arrdata = data.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                ListInfo.Clear();
                foreach (string sd in arrdata)
                {
                    if (!string.IsNullOrEmpty(sd))
                    {
                        PrevInfo = Parse(sd);
                        ListInfo.Add(PrevInfo);
                    }
                }
                return ListInfo;
            }
            catch
            {
                return ListInfo;
            }
        }

        /// <summary>
        /// Parse Sina data to stock Info
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static StockInfo Parse(string content)
        {
            // var hq_str_sh600066 = "宇通客车,9.27,9.35,9.76,9.80,9.27,9.77,9.78,4567858,44306952,3100,9.77,1200,9.76,20500,9.75,1400,9.74,15300,9.73,10030,9.78,28093,9.79,156827,9.80,2800,9.81,6400,9.82,2009-01-09,15:03:32";
            int start = content.IndexOf('"') + 1;
            int end = content.IndexOf('"', start);
            string input = content.Substring(start, end - start);
            string[] temp = input.Split(',');
            if (temp.Length < 32)
            {
                return null;
            }
            StockInfo info = new StockInfo();
            info.Name = temp[0];
            info.TodayOpen = decimal.Parse(temp[1]);
            info.YesterdayClose = decimal.Parse(temp[2]);
            info.Current = decimal.Parse(temp[3]);
            info.High = decimal.Parse(temp[4]);
            info.Low = decimal.Parse(temp[5]);
            info.Buy = decimal.Parse(temp[6]);
            info.Sell = decimal.Parse(temp[7]);
            info.VolAmount = int.Parse(temp[8]);
            info.VolMoney = decimal.Parse(temp[9]);
            info.BuyList = new List<GoodsInfo>(5);
            int index = 10;
            for (int i = 0; i < 5; i++)
            {
                GoodsInfo goods = new GoodsInfo();
                goods.State = GoodsState.Buy;
                goods.Amount = int.Parse(temp[index]);
                index++;
                goods.Price = decimal.Parse(temp[index]);
                index++;
                info.BuyList.Add(goods);
            }
            info.SellList = new List<GoodsInfo>(5);

            for (int i = 0; i < 5; i++)
            {
                GoodsInfo goods = new GoodsInfo();
                goods.State = GoodsState.Sell;
                goods.Amount = int.Parse(temp[index]);
                index++;
                goods.Price = decimal.Parse(temp[index]);
                index++;
                info.SellList.Add(goods);
            }
            info.Time = DateTime.Parse(temp[30] + " " + temp[31]);
            return info;
        }

        #endregion
    }
}
