﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GuPiao
{
    public class GoodsInfo
    {
        public int Amount
        { get; set; }
        public decimal Price
        {
            get;
            set;
        }
        public GoodsState State { get; set; }
    }
}
