﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GuPiao
{
    public interface IDataService
    {
        StockInfo GetCurrentStockInfo(string stockCode);
        List<StockInfo> GetCurrentStockInfo(List<string> stockCode);
    }
}
