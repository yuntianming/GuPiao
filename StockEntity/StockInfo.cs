﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GuPiao
{
    public class StockInfo
    {
        /*http://hq.sinajs.cn/list=sh600066  sh上海 sz深圳
     * 0：”大秦铁路”，股票名字；
   1：”27.55″，今日开盘价；
   2：”27.25″，昨日收盘价；
   3：”26.91″，当前价格；//时间结束后也就是收盘价了
   4：”27.55″，今日最高价；
   5：”26.20″，今日最低价；
   6：”26.91″，竞买价，即“买一”报价；
   7：”26.92″，竞卖价，即“卖一”报价；
   8：”22114263″，成交的股票数，由于股票交易以一百股为基本单位，所以在使用时，通常把该值除以一百；
   9：”589824680″，成交金额，单位为“元”，为了一目了然，通常以“万元”为成交金额的单位，所以通常把该值除以一万；
   10：”4695″，“买一”申请4695股，即47手；
   11：”26.91″，“买一”报价；
   12：”57590″，“买二”
   13：”26.90″，“买二”
   14：”14700″，“买三”
   15：”26.89″，“买三”
   16：”14300″，“买四”
   17：”26.88″，“买四”
   18：”15100″，“买五”
   19：”26.87″，“买五”
   20：”3100″，“卖一”申报3100股，即31手；
   21：”26.92″，“卖一”报价
   (22, 23), (24, 25), (26,27), (28, 29)分别为“卖二”至“卖四的情况”
   30：”2008-01-11″，日期；
   31：”15:05:32″，时间；
   */
        /// <summary>
        /// 股票名称
        /// </summary>
        public string Name
        {
            get;
            set;
        }
        
        /// <summary>
        /// 今日开盘价
        /// </summary>
        public decimal TodayOpen
        {
            get;
            set;
        }

        /// <summary>
        /// 昨日收盘价
        /// </summary>
        public decimal YesterdayClose
        {
            get;
            set;
        }

        /// <summary>
        /// 当前价格,时间结束后也就是收盘价了
        /// </summary>
        public decimal Current
        {
            get;
            set;
        }

        /// <summary>
        /// 今日最高价
        /// </summary>
        public decimal High
        {
            get;
            set;
        }

        /// <summary>
        /// 今日最低价
        /// </summary>
        public decimal Low
        { get; set; }

        /// <summary>
        /// 竟买价 买1
        /// </summary>
        public decimal Buy
        { get; set; }

        /// <summary>
        /// 竟卖价 卖1
        /// </summary>
        public decimal Sell { get; set; }

        /// <summary>
        /// 成交数 单位股数 通常除于100成为手
        /// </summary>
        public int VolAmount { get; set; }

        /// <summary>
        /// 成交多少钱,单位元
        /// </summary>
        public decimal VolMoney { get; set; }

        /// <summary>
        /// 新浪是可以看到5个,5档看盘 ,买1-买5
        /// </summary>
        public List<GoodsInfo> BuyList { get; set; }

        /// <summary>
        /// 卖1－卖5
        /// </summary>
        public List<GoodsInfo> SellList { get; set; }

        /// <summary>
        /// Date and Time
        /// </summary>
        public DateTime Time { get; set; }

        public override string ToString()
        {
            return Name + ": " + VolAmount + ":" + Current;
        }

        /// <summary>
        /// 用来初始化
        /// </summary>
        public StockInfo() 
        { 
        
        }
    
    }
}
